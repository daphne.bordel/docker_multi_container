import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import {Navbar} from "react-bootstrap";
import Home from './components/Home';
import Movies from "./components/Movies";

function App() {
  return (
    <Router>
      <>
        <Navbar bg="dark" variant="dark" className='mb-4'>
            <Navbar.Brand href="/">
              Ma cinémathèque
            </Navbar.Brand>
          </Navbar>
        </>
        <div className="container">
          {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
          <Switch>
            <Route path='/movies'><Movies /></Route>
            <Route path="/" component={Home}/>
          </Switch>
        </div>
    </Router>
  );
}

export default App;
