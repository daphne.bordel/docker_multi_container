import {useState, useEffect} from "react";
import {useRouteMatch, useHistory} from "react-router-dom";

function Movie(){
    const [movie, setMovie] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState('');
    const [id, setId] = useState(null);

    let match = useRouteMatch();
    let history = useHistory();

    useEffect(() => {
        if (!id && match.params.id){
            setId(match.params.id);
        }

        if (id && !movie){
            const getMovieById = async function(){
                const response = await fetch(`http://localhost:8000/api/movies/${id}`);
                if (response.ok){
                    const data = await response.json();
                    setMovie(data);
                    setLoading(false);
                } else {
                    setError("Request failed")
                }
            }
            getMovieById();
        }
    },[id, movie, match.params.id]);

    const returnPage = () => {
        history.goBack()
    }

    if (error !== '') return <div>{error}</div>

    if (loading) return <div>Loading data...</div>

    return(
        <div className="col-md-12">
        <button className="btn btn-secondary" onClick={() => returnPage()}>{`<- Retour`}</button>
        {movie ?
            <div className="row pt-4">
                <div className="col-sm-12">
                    <h1>{movie.Title} ({movie.Year}), de {movie.Director} ({movie.Country})</h1>
                </div>
                <div className="col-sm-12 mb-2">
                    <div className="offset-md-2 col-md-8">
                        <img src={`${movie.Images[0]}`} alt={`illustration film ${movie.Title}`}/>
                    </div>
                </div>
                <div className="col-sm-12 mb-2">
                    <b>Genre : </b>
                    <span>
                        {movie.Genre}
                    </span>
                </div>
                <div className="col-sm-12 mb-2">
                    <b>Acteurs : </b>
                    <span>
                        {movie.Actors}
                    </span>
                </div>
                <div className="col-md-12 mb-2">
                    <b>Résumé : </b>
                    <span>
                        {movie.Plot}
                    </span>
                </div>
            </div>
        :
            <div className="row mt-4">
                Aucun film trouvé
            </div>
        }</div>
    )
}

export default Movie;