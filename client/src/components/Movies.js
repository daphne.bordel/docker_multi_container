import {useState, useEffect} from "react";
import {Switch, Route, useRouteMatch, Link} from "react-router-dom";
import Movie from './Movie';

const Movies = (props) => {
    let match = useRouteMatch();
    
    const [movies, setMovies] = useState([]);
    const [error, setError] = useState('');
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        if (movies.length === 0){
            const getAllMovies = async function(){
                const response = await fetch('http://localhost:8000/api/movies');
                if (response.ok){
                    const data = await response.json();
                    setMovies(data);
                    setLoading(false);
                } else {
                    setError("Request failed")
                }
            }
            getAllMovies();
        }
    },[movies.length]);
    
    if (error !== '') return <div>{error}</div>
    if (loading) return <div>Loading...</div>

    return(
        <Switch>
            <Route path={`${match.path}/:id`}>
                <Movie />
            </Route>
            <Route path={match.path}>
                <h2>Liste de films</h2>
                <ul className="row mt-4">
                    {movies.length > 0 && movies.map((movie,index) => (                       
                        <li key={index}>
                            <Link to={`${match.path}/${movie._id}`}>{movie.Title}</Link>
                        </li>
                    ))}
                </ul>
            </Route>
        </Switch>
    )
}

export default Movies;