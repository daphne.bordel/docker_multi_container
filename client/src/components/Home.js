
import {Route} from "react-router-dom";
import Movies from './Movies';

const Home = () => {
    return(
        <div>
            <h1>Accueil</h1>
            <p>Application de test pour la réalisation d'une application multi-container par le biais de Docker.</p>
            <p>Elle dispose d'une application front-office en React JS, d'un serveur node JS et d'une base de données mongoDB.</p>
            <p>Sur ce site vous disposez d'une liste de films. Vous pourrez voir les informations sur les films listées sur cette page :</p>
            <a href="/movies">Liste de films</a>
                <Route path="/movies" component={Movies}/>
        </div>
    )
}

export default Home;