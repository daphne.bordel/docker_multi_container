const express = require('express');
const mongoose = require('mongoose');
const routes = require('./routes');
const cors = require('cors');

const urlMongo = 'mongodb+srv://test:test@cluster0.ku6vw.mongodb.net/test_db?retryWrites=true&w=majority';

mongoose
    .connect(urlMongo, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        user: 'test',
        password: 'test'
    })
    .then(() => {
        const app = express();
        app.use(express.json());
        app.use(cors());
        app.use("/api",routes);
        app.listen(8000, () => {
            console.log('Server has started on port ',8000);
        });
    });