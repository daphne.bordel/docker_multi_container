const express = require("express");
const Movie = require("./models/Movie");
const router = express.Router();

// Get movie by id
router.get("/movies/:id", async(req, res) => {
    try {
        const movie = await Movie.findOne({ _id: req.params.id});
        res.send(movie);
    } catch {
        res.status(404);
        res.send({error : "Movie doesn't exist"});
    }
});
// Get all movies
router.get("/movies", async(req, res) => {
    try {
        const movies = await Movie.find();
        res.send(movies);
    } catch {
        res.status(404);
        res.send({error: "Movies not found"});
    }
});

module.exports = router;